import React, {PureComponent} from 'react';
import cssClasses from './App.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
import withClass from '../hoc/withClass';
import Aux from '../hoc/AuxComponent';

// PureComponent will handle shouldComponentUpdate() implementation - it compares if state changed somehow
class App extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            persons: [
                {name: 'Max', age: 28, id: 1},
                {name: 'Paul', age: 29, id: 2},
                {name: 'Peter', age: 28, id: 3}
            ],
            showPersons: false,
            toggleClickedCounter: 0
        };

        console.log('[App.js] Inside Constructor', props);
    }

    componentWillMount() {
        console.log('[App.js] componentWillMount()');
    }

    componentDidMount() {
        console.log('[App.js] componentDidMount()');
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log('[UPDATE App.js] shouldComponentUpdate()', nextProps, nextState);
    //     return nextState.persons !== this.state.persons ||
    //         nextState.showPersons !== this.state.showPersons;
    // }

    componentWillUpdate(nextProps, nextState) {
        console.log('[UPDATE App.js] componentWillUpdate()', nextProps, nextState);
    }

    componentDidUpdate() {
        console.log('[UPDATE App.js] componentDidUpdate()');
    }

    // state = {   // reserved word, used to manage internal components data
    //     // it is very good to use id and map it to KEY property, so React can handle arrays far more
    //     // efficiently - it doesnt have to render always the whole list, it just compares the list by IDs
    //     persons: [
    //         {name: 'Max', age: 28, id: 1},
    //         {name: 'Paul', age: 29, id: 2},
    //         {name: 'Peter', age: 28, id: 3}
    //     ],
    //     showPersons: false
    // };

    switchNameHandler = (newName) => {
        // DONT DO THIS: this.state.persons[0].name = 'Maximilian'
        this.setState({
            persons: [
                {name: newName, age: 28, id: 1},
                {name: 'Paul', age: 29, id: 2},
                {name: 'Peter', age: 29, id: 3}
            ]
        })
    };

    nameChangedHandler = (event, id) => {
        const personIndex = this.state.persons.findIndex(p => {
            return p.id === id;
        });

        //const person = this.state.persons[personIndex]; // direct reference, shouldnt modify directly
        //const person = Object.assign({}, this.state.persons[personIndex]);    // good approach
        const person = {
            ...this.state.persons[personIndex]
        };
        person.name = event.target.value;

        const persons = [...this.state.persons];
        persons[personIndex] = person;

        this.setState({persons: persons});
    };

    deletePersonHandler = (personIndex) => {
        //const persons = this.state.persons.slice();   // old way of copy
        const persons = [...this.state.persons];
        persons.splice(personIndex, 1);
        this.setState({persons: persons});
    };

    togglePersonsHandler = () => {
        const doesShow = this.state.showPersons;

        // we should do it like this because:
        // 1. setState is asynchronous, so state can be changed meanwhile we are changing state,
        // so we can not rely on toggleClickedCounter: this.state.toggleClickedCounter+1,
        // because 'this.state' can be changed meanwhile
        // 2. this pattern with usage of prevState makes state immutable and we can add + 1,
        // because prevState can not be accessed from elsewhere
        // 3. toggleClickedCounter: this.state.toggleClickedCounter+1 is not the correct way
        // 4. it is better to use when we modify state also from someone else
        this.setState((prevState, props) => {
            return {
                showPersons: !doesShow,
                toggleClickedCounter: prevState.toggleClickedCounter + 1
            }
        });
    };

    render() {
        console.log('[App.js] render()');
        let persons = null;

        if (this.state.showPersons) {
            persons = <Persons
                persons={this.state.persons}
                clicked={this.deletePersonHandler}
                changed={this.nameChangedHandler}/>;
        }

        return (
            <Aux>
                <button onClick={() => {this.setState({showPersons: true})}}>Show Persons</button>
                <p>Clicks count: {this.state.toggleClickedCounter}</p>
                <Cockpit
                    appTitle={this.props.title}
                    showPersons={this.state.showPersons}
                    persons={this.state.persons}
                    clicked={this.togglePersonsHandler}
                    resetClicked={this.switchNameHandler}/>
                {persons}

                {/*Best practise - use as much as possible functional components
                and use class based component to manage them and manage the state of app*/}
            </Aux>
        );
    }
}

export default withClass(App, cssClasses.App);
