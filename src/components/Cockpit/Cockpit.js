import React from 'react';
import cssClasses from './Cockpit.css'
import Aux from '../../hoc/AuxComponent';

const cockpit = (props) => {
    const appStyleClasses = [];
    let buttonClass = cssClasses.Button;

    if (props.persons.length <= 2) {
        appStyleClasses.push(cssClasses.red);
    }
    if (props.persons.length <= 1) {
        appStyleClasses.push(cssClasses.bold);
    }

    if (props.showPersons) {
        buttonClass = [cssClasses.Red, cssClasses.Button].join(' ');
    }

    return (
        <Aux>
            <h1>Hi, Im a React App: {props.appTitle}</h1>
            <p className={appStyleClasses.join(' ')}>This is really working!</p>

            <button
                className={buttonClass}
                onClick={() => props.resetClicked('Maximilian!! Restored names')}
                key="b1">
                Switch name
            </button>

            <button
                className={buttonClass}
                onClick={props.clicked}
                key="b2">
                Toggle persons
            </button>
        </Aux>
    );
};

export default cockpit;
