import React, {Component} from 'react';
import PropTypes from 'prop-types';

import cssClasses from './Person.css';
import withClass from '../../../hoc/withClass';
import Aux from '../../../hoc/AuxComponent';

// by default we should use functions rather than classes - less states, less complications
class Person extends Component {
    constructor(props) {
        super(props);

        console.log('[Person.js] Inside Constructor', props);
    }

    componentWillMount() {
        console.log('[Person.js] componentWillMount()');
    }

    componentDidMount() {
        console.log('[Person.js] componentDidMount()');
        if (this.props.position === 0) {
            this.inputElement.focus();  // used often for focusing or media playback
            // DO NOT DO STH LIKE THIS: this.inputElement.style.sth
        }
    }

    render() {
        console.log('[Person.js] render()');

        return (
            <Aux>
                <p onClick={this.props.click}>Im {this.props.name} and I am {this.props.age}!</p>
                <p>{this.props.children}</p>
                <input
                    ref={(inp) => {this.inputElement = inp}}
                    type="text"
                    onChange={this.props.changed}
                    value={this.props.name}/>
            </Aux>
        );

        // return [
        //     <p key="1" onClick={this.props.click}>Im {this.props.name} and I am {this.props.age}!</p>,
        //     <p key="2">{this.props.children}</p>,
        //     <input key="3" type="text" onChange={this.props.changed} value={this.props.name}/>
        // ];
    }
}

Person.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number,
    click: PropTypes.func,
    changed: PropTypes.func
};

export default withClass(Person, cssClasses.Person);
