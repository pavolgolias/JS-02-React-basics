﻿## JS-02 - React basics example project
This is an example React project covering multiple React features. Based on UDEMY course: https://www.udemy.com/react-the-complete-guide-incl-redux/.

#### See all branches for features examples

#### Used commands and libs:
* create-react-app my-app-name
* npm install
* npm start
* npm install --save radium
* npm run eject
* npm install --save prop-types

